package main

import (
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"runtime/pprof"
	"time"

	_ "github.com/augustoroman/hexdump"

	"gitlab.com/gitlab-org/database-team/query-intercepter/analyzer"
	"gitlab.com/gitlab-org/database-team/query-intercepter/analyzer/postgres"
	"gitlab.com/gitlab-org/database-team/query-intercepter/netmux"
	"gitlab.com/gitlab-org/database-team/query-intercepter/processor"
	"gitlab.com/gitlab-org/database-team/query-intercepter/registry"
)

var listenPort = flag.Int("listenPort", 5430, "port for proxy to listen on")
var remotePort = flag.Int("remotePort", 5432, "port for proxy to connect to")
var remoteAddr = flag.String("remoteAddr", "localhost", "host for proxy to connect to")
var debug = flag.Bool("debug", false, "use to turn out enhanced diagnostic output")
var adapter = flag.String("adapter", "postgres", "select DB adapter. options are: postgres")
var logToStdOut = flag.Bool("logToStdout", false, "logs all queries to standard out")
var httpListenStr = flag.String("httpListenStr", "0.0.0.0:8080", "hostname:port to listen to for HTTP requests")
var cpuprofile = flag.Bool("cpuprofile", false, "if set, profiles CPU and writes results to cpu.prof")
var memprofile = flag.Bool("memprofile", false, "if set, profiles memory usage and writes results to mem.prof")

var queryAnalyzer analyzer.QueryAnalyzer

var queryRegistry registry.QueryRegistry

func readQueries(c chan analyzer.AnalyzerStatement) {
	for {
		aStmt, more := <-c
		if !more {
			return
		}
		if aStmt.Error != nil {
			fmt.Println(aStmt.Error)
			continue
		}
		if *logToStdOut {
			fmt.Printf("\u001b[37m%s\u001b[0m\n", aStmt.Statement)
		}
		normalized, err := processor.CollapseInNormalize(aStmt.Statement)
		if err != nil {
			fmt.Println("Unable to normalize query", aStmt.Statement, err)
			continue
		}
		fingerprint, err := processor.Fingerprint(normalized)
		if err != nil {
			fmt.Println("Unable to fingerprint normalized query", normalized, err)
			continue
		}
		queryRegistry.LockForWrite()
		err = queryRegistry.AddQuery(&registry.QueryData{
			OriginalStatement:   aStmt.Statement,
			NormalizedStatement: normalized,
			Fingerprint:         fingerprint,
		})
		if err != nil {
			fmt.Println(err)
		}
		queryRegistry.UnlockForWrite()
	}

}

func serviceIncomingConnection(c net.Conn) {
	dialer := net.Dialer{Timeout: 10 * time.Second}
	remoteConn, err := dialer.Dial("tcp",
		fmt.Sprintf("%s:%d", *remoteAddr, *remotePort),
	)
	if err != nil {
		fmt.Println(err)
		c.Close()
		return
	}

	newMux := netmux.IOMuxer{
		Local:       c,
		Remote:      remoteConn,
		QueryReader: queryAnalyzer,
		Debug:       *debug,
	}
	connErr := newMux.Mux()
	fmt.Println("Connection closed with", connErr)
}

func main() {
	flag.Parse()
	if *adapter == "postgres" {
		queryAnalyzer = &postgres.PostgresAnalyzer{}
	} else {
		fmt.Println("This adapter is not supported. Use the -help flag for a list of available adapters.")
		os.Exit(-1)
	}
	qStrChan := make(chan analyzer.AnalyzerStatement, 128)
	queryAnalyzer.SetNotificationChannel(qStrChan)
	go readQueries(qStrChan)
	var err error
	queryRegistry, err = registry.Open()
	if err != nil {
		panic(err)
	}
	queryRegistry.BlockOnReadLock = true
	http.Handle("/", &queryRegistry)
	go http.ListenAndServe(*httpListenStr, nil)
	listener, err := net.Listen("tcp",
		fmt.Sprintf("%s:%d", "0.0.0.0", *listenPort),
	)
	if err != nil {
		panic(err)
	}
	if *cpuprofile {
		cf, err := os.Create("cpu.prof")
		if err != nil {
			log.Fatalln(err)
		}
		pprof.StartCPUProfile(cf)
		defer pprof.StopCPUProfile()
	}
	sigIntChan := make(chan os.Signal, 1)
	signal.Notify(sigIntChan, os.Interrupt)
	go func() {
		for {
			fmt.Println("Listening for connection on port", *listenPort)
			fmt.Printf("Forwarding connection to %s:%d\n", *remoteAddr, *remotePort)
			localConn, err := listener.Accept()
			if err != nil {
				fmt.Println(err)
				continue
			}
			go serviceIncomingConnection(localConn)
		}
	}()
	<-sigIntChan
	log.Println("Got SIGINT, shutting down")
	if *memprofile {
		mf, err := os.Create("mem.prof")
		if err != nil {
			log.Fatalln(err)
		}
		defer mf.Close() // error handling omitted for example
		runtime.GC()     // get up-to-date statistics
		if err := pprof.WriteHeapProfile(mf); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
	}
}
