FROM golang:1.18-alpine as builder

ARG BUILDOS
ARG BUILDARCH

WORKDIR /app
COPY . ./

RUN apk add --no-cache gcc libc-dev
RUN go get -d -v
RUN go mod tidy
RUN env GOOS=$BUILDOS GOARCH=$BUILDARCH go build -o query-interceptor -trimpath -ldflags="-w -s -buildid="

FROM alpine:latest

COPY --from=builder /app/query-interceptor /usr/bin/query-interceptor

EXPOSE 5430
EXPOSE 8080

ENTRYPOINT ["/usr/bin/env"]
CMD ["/usr/bin/query-interceptor"]
