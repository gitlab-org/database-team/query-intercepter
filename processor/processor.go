package processor

import (
	"fmt"
	"reflect"

	pg_query "github.com/pganalyze/pg_query_go/v2"
)

func CollapseInNormalize(query string) (string, error) {
	queryNormalized, err := pg_query.Normalize(query)
	if err != nil {
		return "", err
	}
	tree, err := pg_query.Parse(queryNormalized)
	if err != nil {
		return "", err
	}
	for _, stmt := range tree.Stmts {
		aExprsChanged := 0
		results := findNodeOfType[*pg_query.A_Expr](stmt)
		for _, v := range *results {
			aRef, ok := v.(*pg_query.A_Expr)
			if !ok {
				fmt.Printf("weird, got wrong type %T\n", v)
				continue
			}
			aExprsChanged += truncateAexprInList(aRef)
		}
		if aExprsChanged > 0 {
			realignParamNumbers(stmt)
		}
	}
	return pg_query.Deparse(tree)
}

func realignParamNumbers(stmt *pg_query.RawStmt) {
	paramNum := int32(1)
	paramRefs := findNodeOfType[*pg_query.ParamRef](stmt)
	for _, v := range *paramRefs {
		paramRef, ok := v.(*pg_query.ParamRef)
		if !ok {
			continue
		}
		paramRef.Number = paramNum
		paramNum++
	}
}

func truncateAexprInList(expr *pg_query.A_Expr) int {
	if expr.Kind != pg_query.A_Expr_Kind_AEXPR_IN {
		return 0
	}
	list, ok := expr.Rexpr.Node.(*pg_query.Node_List)
	if !ok {
		fmt.Println("Not a list?")
		return 0
	}
	list.List.Items = list.List.Items[0:1]
	return 1
}

func findNodeOfType[T any](rootNode any) *[]interface{} {
	var results []interface{}
	var p T
	recurse(rootNode, reflect.TypeOf(p), &results)
	return &results
}

func recurse(v interface{}, t reflect.Type, results *[]interface{}) {
	if reflect.TypeOf(v) == t {
		*results = append(*results, v)
	}

	switch reflect.ValueOf(v).Kind() {
	case reflect.Array, reflect.Slice:
		s := reflect.ValueOf(v)
		for i := 0; i < s.Len(); i++ {
			recurse(s.Index(i).Interface(), t, results)
		}
	case reflect.Map:
		keys := reflect.ValueOf(v).MapKeys()
		for _, k := range keys {
			recurse(reflect.ValueOf(v).MapIndex(k).Interface(), t, results)
		}
	case reflect.Struct:
		s := reflect.ValueOf(v)
		for i := 0; i < s.NumField(); i++ {
			field := s.Field(i)
			if field.CanInterface() {
				recurse(field.Interface(), t, results)
			}
		}
	case reflect.Ptr:
		if !reflect.ValueOf(v).IsNil() {
			recurse(reflect.ValueOf(v).Elem().Interface(), t, results)
		}
	}
}

func Normalize(in string) (string, error) {
	return pg_query.Normalize(in)
}

func Fingerprint(query string) (string, error) {
	return pg_query.Fingerprint(query)
}
