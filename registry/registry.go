package registry

import (
	"encoding/binary"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"sync"

	"google.golang.org/protobuf/proto"
)

const registryFileName = "query_registry.bin"

var ErrReadLocked = fmt.Errorf("query registry is read locked")

type QueryRegistry struct {
	FileName          string
	QueryFingerprints map[string]struct{}
	BlockOnReadLock   bool
	writeHandle       *os.File
	readHandle        *os.File
	readLocker        *sync.RWMutex
}

func (t *QueryRegistry) LockForWrite() {
	t.readLocker.Lock()
}

func (t *QueryRegistry) UnlockForWrite() {
	t.readLocker.Unlock()
}

func (t *QueryRegistry) ReadFromRegistry() (*QueryData, error) {
	if !t.BlockOnReadLock && !t.readLocker.TryRLock() {
		return nil, ErrReadLocked
	}
	t.readLocker.RLock()
	defer t.readLocker.RUnlock()
	lenBuf := make([]byte, binary.MaxVarintLen32)
	// No message in the registry would ever be this big
	msgBuf := make([]byte, 1024*512)
	_, err := t.readHandle.Read(lenBuf)
	if err != nil {
		return nil, err
	}
	msgLen := binary.BigEndian.Uint32(lenBuf)
	n, err := t.readHandle.Read(msgBuf[0:msgLen])
	if err != nil {
		return nil, err
	}
	if uint32(n) != msgLen {
		return nil, fmt.Errorf("read mismatch: expected %d but got %d", msgLen, n)
	}
	var qd QueryData
	err = proto.Unmarshal(msgBuf[0:msgLen], &qd)
	return &qd, err
}

func (t *QueryRegistry) AddQuery(d *QueryData) error {
	numBuf := make([]byte, binary.MaxVarintLen32)
	_, exists := t.QueryFingerprints[d.Fingerprint]
	if exists {
		return nil
	}
	t.QueryFingerprints[d.Fingerprint] = struct{}{}
	data, err := proto.Marshal(d)
	if err != nil {
		return err
	}
	binary.BigEndian.PutUint32(numBuf, uint32(len(data)))
	_, err = t.writeHandle.Write(numBuf)
	if err != nil {
		return err
	}
	_, err = t.writeHandle.Write(data)
	return err
}

func (t *QueryRegistry) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var err error
	t.readHandle, err = os.Open(registryFileName)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(fmt.Sprintf("%s", err)))
		return
	}
	defer t.readHandle.Close()
	w.WriteHeader(200)
	for {
		data, err := t.ReadFromRegistry()
		if err != nil {
			if err == io.EOF {
				return
			}
			w.Write([]byte(fmt.Sprintf("REGISTRY ERROR: %s", err)))
			return
		}
		record, err := json.Marshal(data)
		if err != nil {
			w.Write([]byte("JSON MARSHALING ERROR\n"))
			continue
		}
		w.Write(record)
		w.Write([]byte("\n"))
	}
}

func Open() (QueryRegistry, error) {
	var err error
	var registry QueryRegistry
	registry.writeHandle, err = os.Create(registryFileName)
	if err != nil {
		return registry, err
	}
	registry.readLocker = &sync.RWMutex{}
	registry.QueryFingerprints = make(map[string]struct{})
	return registry, err
}
