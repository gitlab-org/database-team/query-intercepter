package processor

import (
	"testing"
)

var benchmarkQuery = `UPDATE books SET title='Widgets', author='Sam Jones', year=2000 WHERE id IN (2, 3, 4, 5, 6)`

func TestCollapseInNormalize(t *testing.T) {
	testQueries := []struct {
		Input    string
		Expected string
	}{
		{
			// Basic functionality
			`SELECT * FROM books WHERE id IN (25, 15, 18)`,
			`SELECT * FROM books WHERE id IN ($1)`,
		},
		{
			// Recurses into WHERE statement to find IN statements AND
			// properly rearranges param numbers
			`SELECT * FROM books WHERE id IN (25, 15, 18) OR id IN (35, 36, 76)`,
			`SELECT * FROM books WHERE id IN ($1) OR id IN ($2)`,
		},
		{
			// Doesn't harm VALUES lists
			`INSERT INTO books (title, author, year) VALUES ($1, $2, $3)`,
			`INSERT INTO books (title, author, year) VALUES ($1, $2, $3)`,
		},
		{
			// Doesn't harm SET clauses
			`UPDATE books SET title='Widgets', author='Sam Jones', year=2000 WHERE id IN (2, 3, 4, 5, 6)`,
			`UPDATE books SET title = $1, author = $2, year = $3 WHERE id IN ($4)`,
		},
		{
			// Doesn't harm subqueries
			`SELECT author FROM books WHERE id IN (SELECT id FROM books WHERE year < 2000)`,
			`SELECT author FROM books WHERE id IN (SELECT id FROM books WHERE year < $1)`,
		},
		{
			// Even finds IN clauses in subqueries and collapses them
			`SELECT author FROM books WHERE id IN (SELECT id FROM books WHERE year IN (2000, 2002))`,
			`SELECT author FROM books WHERE id IN (SELECT id FROM books WHERE year IN ($1))`,
		},
	}

	for _, v := range testQueries {
		res, err := CollapseInNormalize(v.Input)
		if err != nil {
			t.Fatal(err)
		}
		t.Log(res)
		if res != v.Expected {
			t.Fatalf("expected %s but got %s", v.Expected, res)
		}
	}

}

func TestFingerprint(t *testing.T) {
	testQueries := []struct {
		Input    string
		Expected string
	}{
		{
			// Basic functionality
			`SELECT * FROM books WHERE id IN (25, 15, 18)`,
			"61f7da5f83b9bf1d",
		},
		{
			// Don't care about whitespace
			`SELECT *   FROM books  WHERE id IN (25, 15, 18)`,
			"61f7da5f83b9bf1d",
		},
	}
	for _, v := range testQueries {
		res, err := Fingerprint(v.Input)
		if err != nil {
			t.Fatal(err)
		}
		t.Log(res)
		if res != v.Expected {
			t.Fatalf("expected %s but got %s", v.Expected, res)
		}
	}
}

func BenchmarkCollapseInNormalize(b *testing.B) {
	for n := 0; n < b.N; n++ {
		CollapseInNormalize(benchmarkQuery)
	}
}

func BenchmarkFingerprint(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Fingerprint(benchmarkQuery)
	}
}

func BenchmarkNormalize(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Normalize(benchmarkQuery)
	}
}

func TestNormalize(t *testing.T) {
	expected := `UPDATE books SET title=$1, author=$2, year=$3 WHERE id IN ($4, $5, $6, $7, $8)`
	res, err := Normalize(benchmarkQuery)
	if err != nil {
		t.Fatal(err)
	}
	t.Log(res)
	if res != expected {
		t.Fatalf("expected %s, got %s", expected, res)
	}
}
