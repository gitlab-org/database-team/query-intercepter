require "pg"

conn = PG.connect(
  host: "localhost",
  dbname: "gitlabhq_development",
  user: "postgres",
  port: 5430
)

# Run a simple query to check the connection
result = conn.exec_params("SELECT * FROM sent_notifications WHERE id = $1", [3])
result = conn.exec("SELECT version();")
puts result

# Close the connection
conn.close
