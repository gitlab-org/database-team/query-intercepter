package netmux

import (
	"fmt"
	"io"
	"net"

	"github.com/augustoroman/hexdump"
)

type IOMuxer struct {
	Local       net.Conn
	Remote      net.Conn
	QueryReader io.Writer
	Debug       bool
}

func (t *IOMuxer) readTrafficPipe(r io.Reader) {
	dumpBuf := make([]byte, 4096)
	for {
		n, err := r.Read(dumpBuf)
		if err != nil {
			break
		}
		fmt.Println(hexdump.Dump(dumpBuf[0:n]))
	}
}

func (t IOMuxer) Mux() error {
	upstreamTee := io.TeeReader(t.Local, t.QueryReader)
	downstreamTee := io.TeeReader(t.Remote, t.QueryReader)
	if t.Debug {
		upstreamReader, upstreamWriter := io.Pipe()
		downstreamReader, downstreamWriter := io.Pipe()
		defer upstreamWriter.Close()
		defer downstreamWriter.Close()
		upstreamTee = io.TeeReader(io.TeeReader(t.Local, t.QueryReader), upstreamWriter)
		downstreamTee = io.TeeReader(io.TeeReader(t.Remote, t.QueryReader), downstreamWriter)
		go t.readTrafficPipe(upstreamReader)
		go t.readTrafficPipe(downstreamReader)

	}

	go io.Copy(t.Local, downstreamTee)
	_, err := io.Copy(t.Remote, upstreamTee)
	return err
}
