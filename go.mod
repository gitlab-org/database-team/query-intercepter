module gitlab.com/gitlab-org/database-team/query-intercepter

go 1.18

require (
	github.com/augustoroman/hexdump v0.0.0-20190827031536-6506f4163e93
	github.com/pganalyze/pg_query_go/v2 v2.2.0
	google.golang.org/protobuf v1.28.1
)

require (
	github.com/golang/protobuf v1.5.0 // indirect
	github.com/google/go-cmp v0.5.9 // indirect
)
