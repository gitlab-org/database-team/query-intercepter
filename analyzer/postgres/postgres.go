package postgres

import (
	"bytes"
	"encoding/binary"
	"strings"

	"gitlab.com/gitlab-org/database-team/query-intercepter/analyzer"
)

type queryFlavor uint8

type PostgresAnalyzer struct {
	internalBuffer    bytes.Buffer
	messageDataLength int
	flavor            queryFlavor
	queryString       string
	completeChan      chan analyzer.AnalyzerStatement
}

const BASIC_QUERY_BYTE1 = 0x51
const PARSE_QUERY_BYTE1 = 0x50
const POSTGRES_STATEMENT_TERMINATOR = 0

const (
	basicQuery queryFlavor = iota
	parseQuery
)

func (t *PostgresAnalyzer) Write(packet []byte) (int, error) {
	if t.internalBuffer.Len() == 0 {
		if packet[0] == BASIC_QUERY_BYTE1 {
			t.flavor = basicQuery
		} else if packet[0] == PARSE_QUERY_BYTE1 {
			t.flavor = parseQuery
		} else {
			return 0, nil
		}
		t.messageDataLength = int(binary.BigEndian.Uint32(packet[1:5]))
	}
	n, _ := t.internalBuffer.Write(packet)
	if t.internalBuffer.Len() > analyzer.QuerySizeLimit {
		t.internalBuffer.Reset()
		t.completeChan <- analyzer.AnalyzerStatement{
			Statement: "",
			Error:     analyzer.ErrQueryTooLarge,
		}
		return n, nil
	}
	if t.internalBuffer.Len() >= t.messageDataLength+1 {
		t.analyzeQuery()
		t.internalBuffer.Reset()
		t.completeChan <- analyzer.AnalyzerStatement{
			Statement: t.QueryString(),
			Error:     nil,
		}
	}
	return n, nil
}

func (t *PostgresAnalyzer) SetNotificationChannel(c chan analyzer.AnalyzerStatement) {
	t.completeChan = c
}

func (t *PostgresAnalyzer) AnalysisFinished() chan analyzer.AnalyzerStatement {
	return t.completeChan
}

func (t *PostgresAnalyzer) QueryString() string {
	if len(t.queryString) > 0 && t.queryString[len(t.queryString)-1] != ';' {
		t.queryString += ";"
	}
	return t.queryString
}

func (t *PostgresAnalyzer) analyzeQuery() error {
	if t.flavor == basicQuery {
		return t.analyzeBasicQuery()
	}
	return t.analyzeParseQuery()
}

func (t *PostgresAnalyzer) analyzeBasicQuery() error {
	t.internalBuffer.Next(5)
	stringBuf, err := t.internalBuffer.ReadString(POSTGRES_STATEMENT_TERMINATOR)
	if err != nil {
		return err
	}
	t.queryString = strings.TrimSuffix(stringBuf, "\x00")
	return nil
}

func (t *PostgresAnalyzer) analyzeParseQuery() error {
	var stringBuf string
	t.internalBuffer.Next(5)
	// Discard prepared statements - one possibility is we can cache these
	// and remove them from future recording once we see them.
	stringBuf, err := t.internalBuffer.ReadString(POSTGRES_STATEMENT_TERMINATOR)
	if err != nil {
		return err
	}
	if len(strings.TrimSuffix(stringBuf, "\x00")) > 0 {
		// Just return and do nothing if we see a prepared statement
		return nil
	}
	stringBuf, err = t.internalBuffer.ReadString(POSTGRES_STATEMENT_TERMINATOR)
	if err != nil {
		return err
	}
	t.queryString = strings.TrimSuffix(stringBuf, "\x00")
	return nil
}
