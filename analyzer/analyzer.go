package analyzer

import (
	"fmt"
	"io"
)

const QuerySizeLimit = 4096

var ErrQueryTooLarge = fmt.Errorf("query buffer larger than %d", QuerySizeLimit)

type AnalyzerStatement struct {
	Statement string
	Error     error
}

type QueryAnalyzer interface {
	io.Writer
	SetNotificationChannel(chan AnalyzerStatement)
	AnalysisFinished() chan AnalyzerStatement
}
