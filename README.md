# Query Interceptor

query-interceptor acts as a proxy server between a DBMS client and server that analyzes the traffic originating from the client and produces a log of queries run by the client.

## Uses

* Understand what queries your application runs
* Compare logs over time to detect new queries

## How to use

### Basic Usage

Start the application from the command line, using flags to configure it. Run `./query-interceptor -help` for the most up-to-date list of flags and their default settings.

For example, if we want to connect a local applcation to a Postgres database named `gitlab_db` hosted at foo.gitlab.com listening on port 5432, we could start query-interceptor using the following command:

```bash
./query-intercepter -adapter postgres -listenPort 5430 -remoteAddr foo.gitlab.com -remotePort 5432
```

Then in our (Ruby) application, we could set up a DB connection like so:

```ruby
require "pg"

conn = PG.connect(
  host: "localhost",
  dbname: "gitlab_db",
  user: "gitlab_user",
  port: 5430
)
```

### Viewing Recorded Queries

The application starts an HTTP server, by default the address is 0.0.0.0:8080. The user may visit this address to get an NDJSON representation of all queries recorded so far during the run of the application.

## Future Roadmap

### Short Term

* Ability to load a list of fingerprints of previously-examined queries
* Add a "diff" format to the HTTP view so we can see which queries have changed between runs

### Medium/Long Term
* Only Postgres is currently supported. However, the application has been designed with modularity in mind - we can write a new Go package to analyze any RDBMS traffic for query logging.
* There may be use cases where SSL is necessary on the remote host, therefore the application should be SSL aware.